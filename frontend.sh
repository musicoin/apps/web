git submodule init
git submodule update
rm -rf src/overview/
cd website/
rm -rf build/
git pull origin master
git checkout master
npm install
npm run build
cp -r build/ ../src/overview/
cd ..
